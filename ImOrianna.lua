--[[
    I'M Orianna 1.1 by Klokje
    ================================================================================
 
    Change log:
    1.0  - New Version
         - Own prediction(please test isnt done jet)
         - Need to work smoother

    1.1 
        = Hope to fix the bug
        - Remove Orbwalking , will back in new way next version
]]

if myHero.charName ~= "Orianna" then return end

require "ImLibPrivate" 
require "Prodiction"

qRange = 900
wRange = 240
eRange = 1120
rRange = 350

ballRange = 1335
ballColor = 0x006600
ball = myHero
bMoving = false

local Cooldown = {
    [_Q] = 0,
    [_W] = 0,
    [_E] = 0,
    [_R] = 0,
}

local liandry = 3151
local blackfire = 3188

local DFGSlot = 3128
local HXGSlot = 3146
local BWCSlot = 3144

enemyHeroes = {}
allyHeroes = {}

IsRecaling = false

ignite = nil

lastmovement = nil
Wpm = WayPointManager()


ts = TargetSelector(TARGET_LOW_HP, qRange, DAMAGE_MAGIC, true)


prodictionQ = ProdictManager.GetInstance():AddProdictionObject(_Q, 1000, 1100, 0.250, 80, myHero, function(unit, pos, spell) 
    if ball and GetDistance(pos) < qRange+50 and GetDistance(ball, pos) < 1500 and myHero:CanUseSpell(_Q) == READY and myHero:GetSpellData(_Q).mana < myHero.mana and not bMoving then 
        CastSpell(_Q, pos.x, pos.z)
    end 
 end)
prodictionR = ProdictManager.GetInstance():AddProdictionObject(_R, 350, 20000, 0.250, 80, myHero, function(unit, pos) end)

function OnLoad()
    PrintChat(" >> I'M Orianna 1.1")  
    OrbWalking.Instance("I'M Orianna")

    --Check Ball
    for i = 1, objManager.maxObjects, 1 do
        local obj = objManager:GetObject(i)
        CheckBall(obj)
        CheckRangeColor(obj)
    end

    OriannaConfig = scriptConfig("I'M Orianna", "Orianna") 
    OriannaConfig:addSubMenu("Basic Settings", "Basic")
    OriannaConfig:addSubMenu("Shield Settings", "Shield")
    OriannaConfig:addSubMenu("Auto Settings", "Auto")
    OriannaConfig:addSubMenu("Kill Settings", "Kill")
    OriannaConfig:addSubMenu("Farm Settings", "Farm")
    OriannaConfig:addSubMenu("Draw Settings", "Draw")

    --Basic Config
    OriannaConfig.Basic:addTS(ts)
    OriannaConfig.Basic:addParam("Harass", "Harass", SCRIPT_PARAM_ONKEYDOWN, false, 84) 
    OriannaConfig.Basic:addParam("Combo", "Combo", SCRIPT_PARAM_ONKEYDOWN, false, 32)
    OriannaConfig.Basic:addParam("Burst", "Burst", SCRIPT_PARAM_ONKEYDOWN, false, 86)
    OriannaConfig.Basic:addParam("SCombo", "Smart Ball", SCRIPT_PARAM_ONOFF, true)

    OriannaConfig.Basic:permaShow("Harass") 
    OriannaConfig.Basic:permaShow("Combo") 

    --Shield Config
    for i=1, heroManager.iCount do
        local teammate = heroManager:GetHero(i)
        if teammate.team == myHero.team then 
            local max
            if not teammate.isMe then max = 50 - (SortList[teammate.charName]*5) else max = 100 end
            OriannaConfig.Shield:addParam(teammate.charName, "Shield "..teammate.charName, SCRIPT_PARAM_ONOFF, true) 
            OriannaConfig.Shield:addParam(teammate.charName .. "max", teammate.charName ..": max % health", SCRIPT_PARAM_SLICE, max, 0, 100, 0)
            OriannaConfig.Shield:addParam(teammate.charName .. "CC", teammate.charName ..": Shield CC", SCRIPT_PARAM_ONOFF, teammate.isMe)
            OriannaConfig.Shield:addParam(teammate.charName .. "SS", teammate.charName ..": Shield Skillshot", SCRIPT_PARAM_ONOFF, true)
            OriannaConfig.Shield:addParam("shieldbasic", "", SCRIPT_PARAM_INFO, "")
        end
    end

    OriannaConfig.Shield:addParam("shieldult", "Combo Shield enemy", SCRIPT_PARAM_ONOFF, true)
    OriannaConfig.Shield:addParam("shieldultenemy", "Combo: min enemys", SCRIPT_PARAM_SLICE, 3, 0, 5, 0)
    OriannaConfig.Shield:addParam("shielddmg", "Collision E damage ", SCRIPT_PARAM_ONOFF, true)

    --Auto Config
    OriannaConfig.Auto:addParam("AutoStop", "Disable Auto function", SCRIPT_PARAM_ONKEYTOGGLE, false, 86) 
    OriannaConfig.Auto:addParam("Autohealt", "Auto: min % health", SCRIPT_PARAM_SLICE, 35, 0, 100, 0)
    OriannaConfig.Auto:addParam("AutoQ", "Auto Q", SCRIPT_PARAM_ONOFF, true)
    OriannaConfig.Auto:addParam("AutoQpMana", "Auto Q: min % mana", SCRIPT_PARAM_SLICE, 30, 0, 100, 0)
    OriannaConfig.Auto:addParam("AutoW", "Auto W", SCRIPT_PARAM_ONOFF, true)
    OriannaConfig.Auto:addParam("AutoWpMana", "Auto W: min % mana", SCRIPT_PARAM_SLICE, 70, 0, 100, 0)
    OriannaConfig.Auto:addParam("AutoWenemy", "Auto W: min Enemys", SCRIPT_PARAM_SLICE, 1, 0, 5, 0)
    OriannaConfig.Auto:addParam("AutoR", "Auto R", SCRIPT_PARAM_ONOFF, true)
    OriannaConfig.Auto:addParam("AutoRenemy", "Auto R: min enemys", SCRIPT_PARAM_SLICE, 3, 0, 5, 0)

    OriannaConfig.Auto:permaShow("AutoStop") 

    --Kill Config
    OriannaConfig.Kill:addParam("killCombo", "Kill Combo", SCRIPT_PARAM_ONOFF, true)
    OriannaConfig.Kill:addParam("RCombo", "Use R in killcombo", SCRIPT_PARAM_ONOFF, true)
    OriannaConfig.Kill:addParam("autokillCombo", "Auto Kill Combo", SCRIPT_PARAM_ONOFF, true)

    --Farm Config
    OriannaConfig.Farm:addParam("Farm", "Farm", SCRIPT_PARAM_ONKEYTOGGLE, true, 90) 
    OriannaConfig.Farm:addParam("FarmpMana", "Farm: min % mana", SCRIPT_PARAM_SLICE, 20, 0, 100, 0)
    OriannaConfig.Farm:addParam("UseQ", "Farm: Use Q", SCRIPT_PARAM_ONOFF, true)
    OriannaConfig.Farm:addParam("UseW", "Farm: Use W", SCRIPT_PARAM_ONOFF, true)

    OriannaConfig.Farm:permaShow("Farm") 

    --Draw Config   
    OriannaConfig.Draw:addParam("DrawQ", "Draw Q range", SCRIPT_PARAM_ONOFF, true)
    OriannaConfig.Draw:addParam("DrawQColor", "Q Color", SCRIPT_PARAM_COLOR, {16, 41, 0, 0})
    OriannaConfig.Draw:addParam("DrawW", "Draw W range", SCRIPT_PARAM_ONOFF, true)
    OriannaConfig.Draw:addParam("DrawWColor", "W Color", SCRIPT_PARAM_COLOR, {19, 41, 41, 25})
    OriannaConfig.Draw:addParam("DrawE", "Draw E range", SCRIPT_PARAM_ONOFF, true)
    OriannaConfig.Draw:addParam("DrawEColor", "E Color", SCRIPT_PARAM_COLOR, {5, 15, 25, 19})
    OriannaConfig.Draw:addParam("DrawR", "Draw R range", SCRIPT_PARAM_ONOFF, true)
    OriannaConfig.Draw:addParam("DrawRColor", "R Color", SCRIPT_PARAM_COLOR, {0, 20, 40, 100})
    OriannaConfig.Draw:addParam("DrawBall", "Draw Ball range", SCRIPT_PARAM_ONOFF, true)
    OriannaConfig.Draw:addParam("DrawTarget", "Draw Target", SCRIPT_PARAM_ONOFF, true)
    OriannaConfig.Draw:addParam("DrawTargetColor", "Target Color", SCRIPT_PARAM_COLOR, {255, 51, 82, 255})

    for i = 1, heroManager.iCount do
        local hero = heroManager:GetHero(i)
        if hero.team ~= myHero.team then
            table.insert(enemyHeroes, hero)
        else 
            table.insert(allyHeroes, hero)
        end
    end

     if myHero:GetSpellData(SUMMONER_1).name:find("SummonerDot") then ignite = SUMMONER_1
    elseif myHero:GetSpellData(SUMMONER_2).name:find("SummonerDot") then ignite = SUMMONER_2
    end
end 

function AutoShield()
    if not OriannaConfig.Shield.shieldult then return end 

    for i, ally in pairs(allyHeroes) do
        local count = 0
        if GetDistance(ally) < eRange then
            for i, target in pairs(enemyHeroes) do
                if TargetNear(target, rRange, ally) then 
                    count = count + 1
                    if count >= OriannaConfig.Shield.shieldultenemy and myHero:CanUseSpell(_E) == READY and  GetDistance(ally) < eRange then 
                        CastSpell(_E, ally)
                    end 
                end
            end
        end
    end
end

function OnDash(unit, dash)
    if unit and unit.valid and unit.team == myHero.team and unit.type == myHero.type and myHero:CanUseSpell(_E) == READY and OriannaConfig.Shield.shieldult and GetDistance(unit) <= eRange then 
        local endlocation = dash.target and dash.target or dash.endPos
        if GetDistance(endlocation) <= ballRange then
            local count = 0
            for i, target in pairs(enemyHeroes) do
                if ValidTarget(target, rRange+ballRange) and GetDistance(endlocation, target) <= rRange then
                    count = count + 1
                    if count >= OriannaConfig.Shield.shieldultenemy then
                        CastSpell(_E, unit)
                        return
                    end 
                end 
            end 
        end 
    end 
end 

function OnProcessSpell(object,spell)
    if object~= nil and spell ~= nil and object.isMe then
        if spell.name == "OrianaIzunaCommand" then 
            Cooldown[_Q] = GetGameTimer() + (myHero:GetSpellData(_Q).cd * (1 + myHero.cdr))
        elseif spell.name == "OrianaDissonanceCommand" then 
            Cooldown[_W] = GetGameTimer() + (myHero:GetSpellData(_W).cd * (1 + myHero.cdr))
        elseif spell.name == "OrianaRedactCommand" then 
            Cooldown[_E] = GetGameTimer() + (myHero:GetSpellData(_E).cd * (1 + myHero.cdr))
        elseif spell.name == "OrianaDetonateCommand" then 
            Cooldown[_R] = GetGameTimer() + (myHero:GetSpellData(_R).cd * (1 + myHero.cdr))
        end
    end

    Shield(object, spell)
end

function objectValid(object)
    return object ~= nil and object.valid and not object.dead and object.bTargetable
end

function Shield(object,spell) 
    if not myHero:CanUseSpell(_E) == READY or object.team == myHero.team then return end

    for i, ally in pairs(allyHeroes) do
        if objectValid(ally) and GetDistance(ally) < eRange and OriannaConfig.Shield[ally.charName] and ally.maxHealth *(OriannaConfig.Shield[ally.charName.."max"]/100) >= ally.health then
            if object.type == "obj_AI_Turret" and GetDistance(ally, spell.endPos) < 80 then 
                CastSpell(_E, ally)
            elseif object.type == "obj_AI_Hero" and GetDistance(ally, spell.endPos) < 80  then 
                spelltype = getSpellType(object, spell.name)
                if spelltype == "BAttack" or spelltype == "CAttack" and GetDistance(ally, spell.endPos) < 80  then
                    local onhitdmg = 0
                    local onhittdmg = 0
                    local InfinityEdge = 0
                    local adamage = object:CalcDamage(ally,object.totalDamage)
                    if GetInventoryHaveItem(3186,object) then onhitdmg = getDmg("KITAES",ally,object) end
                    if GetInventoryHaveItem(3114,object) then onhitdmg = onhitdmg+getDmg("MALADY",ally,object) end
                    if GetInventoryHaveItem(3091,object) then onhitdmg = onhitdmg+getDmg("WITSEND",ally,object) end
                    if GetInventoryHaveItem(3057,object) then onhitdmg = onhitdmg+getDmg("SHEEN",ally,object) end
                    if GetInventoryHaveItem(3078,object) then onhitdmg = onhitdmg+getDmg("TRINITY",ally,object) end
                    if GetInventoryHaveItem(3100,object) then onhitdmg = onhitdmg+getDmg("LICHBANE",ally,object) end
                    if GetInventoryHaveItem(3025,object) then onhitdmg = onhitdmg+getDmg("ICEBORN",ally,object) end
                    if GetInventoryHaveItem(3087,object) then onhitdmg = onhitdmg+getDmg("STATIKK",ally,object) end
                    if GetInventoryHaveItem(3153,object) then onhitdmg = onhitdmg+getDmg("RUINEDKING",ally,object) end
                    if GetInventoryHaveItem(3042,object) then onhitdmg = onhitdmg+getDmg("MURAMANA",ally,object) end
                    if GetInventoryHaveItem(3209,object) then onhittdmg = getDmg("SPIRITLIZARD",ally,object) end
                    if GetInventoryHaveItem(3184,object) then onhittdmg = onhittdmg+80 end
                    if spelltype == "BAttack" then
                        onhitdmg = (adamage+onhitdmg)*1.07+onhittdmg
                    else
                        if GetInventoryHaveItem(3031,object) then InfinityEdge = .5 end
                        onhitdmg = (adamage*(2.1+InfinityEdge)+onhitdmg)*1.07+onhittdmg 
                    end
                    if onhitdmg >= ally.health or onhitdmg > ally.maxHealth *0.1 then 
                        CastSpell(_E, ally)
                    end
                elseif spelltype == "Q" or spelltype == "W" or spelltype == "E" or spelltype == "R" or spelltype == "P" or spelltype == "QM" or spelltype == "WM" or spelltype == "EM" then
                    local onhitspelltdmg, onhitspelldmg, onhitspelldmg, muramanadmg, dmg = 0, 0, 0, 0, 0
                    local hitchampion = false
                    local CC,Muramana = false,false,false,false,false
                    local shottype,radius,maxdistance = 0,0,0

                    if GetInventoryHaveItem(3151,object) then onhitspelldmg = getDmg("LIANDRYS",ally,object) end
                    if GetInventoryHaveItem(3188,object) then onhitspelldmg = getDmg("BLACKFIRE",ally,object) end
                    if GetInventoryHaveItem(3209,object) then onhitspelltdmg = getDmg("SPIRITLIZARD",ally,object) end
                    if GetInventoryHaveItem(3042,object) then muramanadmg = getDmg("MURAMANA",ally,object) end
                    CC = skillShield[object.charName][spelltype]["CC"]
                    Muramana = skillShield[object.charName][spelltype]["Muramana"]
                    shottype = skillData[object.charName][spelltype]["type"]
                    radius = skillData[object.charName][spelltype]["radius"]
                    maxdistance = skillData[object.charName][spelltype]["maxdistance"]
                    muramanadmg = Muramana and muramanadmg or 0
                    skilldamage = getDmg(spelltype,ally,object,3,spellLevel)

                    if shottype == 0 then hitchampion = checkhitaoe(object, spell.endPos, 80, ally, 0)
                    elseif shottype == 1 then hitchampion = checkhitlinepass(object, spell.endPos, radius, maxdistance, ally, getHitBox(myHero)*2-10)
                    elseif shottype == 2 then hitchampion = checkhitlinepoint(object, spell.endPos, radius, ally, getHitBox(myHero)*2-10)
                    elseif shottype == 3 then hitchampion = checkhitaoe(object, spell.endPos, radius, ally, getHitBox(myHero)*2-10)
                    elseif shottype == 4 then hitchampion = checkhitcone(object, spell.endPos, radius, maxdistance, ally, getHitBox(myHero)*2-10)
                    elseif shottype == 5 then hitchampion = checkhitwall(object, spell.endPos, radius, maxdistance, ally, getHitBox(myHero)*2-10)
                    elseif shottype == 6 then hitchampion = checkhitlinepass(object, spell.endPos, radius, maxdistance, ally, getHitBox(myHero)*2-10) or checkhitlinepass(object, Vector(object)*2-P2, radius, maxdistance, ally, getHitBox(myHero)*2-10)
                    end
                    dmg = (skilldamage+onhitspelldmg+muramanadmg)*1.07+onhitspelltdmg

                    if ((dmg >= ally.health or dmg > ally.maxHealth *0.1) and shottype == 0)  then
                        CastSpell(_E, ally)
                    end
                    if (CC == 2 and OriannaConfig.Shield[ally.charName .. "CC"]) then 
                        CastSpell(_E, ally)
                    end
                    if (OriannaConfig.Shield[ally.charName .. "SS"] and hitchampion and shottype ~= 0)  then
                        CastSpell(_E, ally)
                    end
                end 
            elseif object.type == "obj_AI_Minion" and GetDistance(ally, spell.endPos) < 80 then 
                local dmg = object:CalcDamage(ally,object.totalDamage)
                if  dmg >= ally.health or dmg > ally.maxHealth *0.1 then
                    CastSpell(_E, ally)
                end
            elseif spell.name:find("SummonerDot") and GetDistance(ally, spell.endPos) < 80 then
                dmg = getDmg("IGNITE",ally,object)
                 if  dmg >= ally.health or dmg > ally.maxHealth *0.1 then
                    CastSpell(_E, ally)
                end
            end

        end
    end
end

function OnSendPacket(p)
    if p.header == 113 then
        lastmovement =  copyPacket(p)
    end
    if p.header == 154 and lastmovement then
        SendPacket(lastmovement)
        lastmovement = nil
    end 

end

function Sorting()
    table.sort(enemyHeroes, function(x,y) 
        local dmgx = myHero:CalcMagicDamage(x, 100)
        local dmgy = myHero:CalcMagicDamage(y, 100)

        dmgx = dmgx/ (1 + (SortList[x.charName]/10) - 0.1)
        dmgy = dmgy/ (1 + (SortList[y.charName]/10) - 0.1)

        local valuex = x.health/dmgx
        local valuey = y.health/dmgy

        return valuex < valuey
        end)

    table.sort(allyHeroes, function(x,y) 
        local mdmgx = myHero:CalcMagicDamage(x, 100)
        local mdmgy = myHero:CalcMagicDamage(y, 100)
        local dmgx = myHero:CalcDamage(x, 100)
        local dmgy = myHero:CalcDamage(y, 100)

        dmgx = (dmgx+mdmgx)/ (1 + (SortList[x.charName]/10) - 0.1)
        dmgy = (dmgy+mdmgy)/ (1 + (SortList[y.charName]/10) - 0.1)

        local valuex = x.health/dmgx
        local valuey = y.health/dmgy

        return valuex < valuey
        end)
end

function CheckBall(obj)
    if obj == nil or obj.name == nil then return end  
    
    if (obj.name:find("Oriana_Ghost_mis") or obj.name:find("Oriana_Ghost_mis_protect") ) then
        bMoving = true
        return
    end

    if obj.name:find("yomu_ring_green") then
        ball = obj
        return
    end

    if obj.name:find("Oriana_Ghost_bind") then
        for i, target in pairs(allyHeroes) do
            if GetDistance(target, obj) < 50 then
                ball = target
           end
        end
    end
end

function CheckRangeColor(obj)
    if obj == nil or obj.name == nil then return end  

    if obj.name == "OrianaBallIndicatorNear.troy" then
        ballColor = ColorARGB(0, 15, 0, 0):ToARGB() --0x006600
    elseif obj.name == "OrianaBallIndicatorMedium.troy" then
        ballColor = ColorARGB(30, 30, 0, 0):ToARGB() 
    elseif obj.name == "OrianaBallIndicatorFar.troy" then
        ballColor = 0xFF0000
    end
end

function OnCreateObj(obj)
    if obj == nil or obj.name == nil then return end  

    CheckRangeColor(obj)
end

function OnGainBuff(unit, buff)  
    --print(buff.name .. " : " .. unit.name)
    if buff.name:lower():find("orianaghost") and buff.name ~= "orianaghostminion" and buff.source.isMe then
        ball = unit
    end    
end 

function OnLoseBuff(unit, buff)
    --if unit.isMe then  
    --end 
    if ball and buff.name:lower():find("orianaghost") and buff.name ~= "orianaghostminion" and ball.networkID == unit.networkID then
        ball = nil
    end
end 

function OnDraw()
    if OriannaConfig.Draw.DrawBall and ball and not ball.isMe then
        DrawCircle(ball.x, ball.y, ball.z, ballRange, ballColor)
    end

    if OriannaConfig.Draw.DrawQ and not myHero.dead and myHero:CanUseSpell(_Q) == READY then
        DrawCircle(myHero.x, myHero.y, myHero.z, qRange+55,   ColorARGB.FromTable(OriannaConfig.Draw.DrawQColor))
    end

    if OriannaConfig.Draw.DrawW and ball and not myHero.dead and myHero:CanUseSpell(_W) == READY then
        DrawCircle(ball.x, ball.y, ball.z, wRange,  ColorARGB.FromTable(OriannaConfig.Draw.DrawWColor))
    end

    if OriannaConfig.Draw.DrawE and not myHero.dead and myHero:CanUseSpell(_E) == READY then
        DrawCircle(myHero.x, myHero.y, myHero.z, eRange,  ColorARGB.FromTable(OriannaConfig.Draw.DrawEColor))
    end

    if OriannaConfig.Draw.DrawR and ball and not myHero.dead and myHero:CanUseSpell(_R) == READY then
        DrawCircle(ball.x, ball.y, ball.z, rRange,  ColorARGB.FromTable(OriannaConfig.Draw.DrawRColor))
    end

    if OriannaConfig.Draw.DrawTarget and not myHero.dead and ts.target then
        DrawCircle(ts.target.x, ts.target.y, ts.target.z, 100,  ColorARGB.FromTable(OriannaConfig.Draw.DrawTargetColor))
    end

    for i, target in pairs(enemyHeroes) do
        DrawCalculation(target)
    end
end

function DrawCalculation(target)
    if not ValidTarget(target, 1400) or myHero.dead then return end

    local qDmg = getDmg("Q", target, player)
    local wDmg = getDmg("W", target, player)
    local eDmg = getDmg("E", target, player)
    local rDmg = getDmg("R", target, player)
    local pDmg = getDmg("P", target, player)
    local aDmg = getDmg("AD",target, player)
    local igniteDmg = (ignite and getDmg("IGNITE", target, player) or 0)
    local dfgDmg = (GetInventorySlotItem(DFGSlot) and getDmg("DFG", target, player) or 0)
    local hxgDmg = (GetInventorySlotItem(HXGSlot) and getDmg("HXG", target, player) or 0)
    local bwcDmg = (GetInventorySlotItem(BWCSlot) and getDmg("BWC", target, player) or 0)
    local extradmg = igniteDmg + dfgDmg + hxgDmg + bwcDmg

    local onspellDmg = (GetInventorySlotItem(liandry) and getDmg("LIANDRYS", target, player) or 0) + (GetInventorySlotItem(blackfire) and getDmg("BLACKFIRE", target, player) or 0)
    local onspellDmg = onspellDmg * 0.5
    local onhitDmg = pDmg + aDmg

    local mDmg = 0
    local mana = 0
    local maxDamage = qDmg + wDmg + rDmg + onspellDmg + onhitDmg + extradmg
    if maxDamage < target.health then return end

    if myHero:CanUseSpell(_Q) == READY and mana <= myHero.mana then   
        mDmg = mDmg + qDmg
        mDmg = mDmg + onspellDmg
        mana = mana + myHero:GetSpellData(_Q).mana
    end
    if myHero:CanUseSpell(_W) == READY and mana <= myHero.mana then   
        mDmg = mDmg + wDmg
        mDmg = mDmg + onspellDmg
        mana = mana + myHero:GetSpellData(_W).mana
    end
    if myHero:CanUseSpell(_R) == READY and mana <= myHero.mana then   
        mDmg = mDmg + rDmg
        mDmg = mDmg + onspellDmg
        mana = mana + myHero:GetSpellData(_R).mana
    end
    if ignite ~= nil and myHero:CanUseSpell(ignite) == READY then   
        mDmg = mDmg + igniteDmg
    end
    if myHero:CanUseSpell(DFGSlot) == READY then   
        mDmg = mDmg + dfgDmg
    end
    if myHero:CanUseSpell(HXGSlot) == READY then   
        mDmg = mDmg + hxgDmg
    end
    if myHero:CanUseSpell(BWCSlot) == READY then   
        mDmg = mDmg + bwcDmg
    end

    local pos = WorldToScreen(D3DXVECTOR3(target.x, target.y, target.z))
    if mDmg >= target.health then
        Message.DrawTextWithBorder("Kill it!", 20, pos.x- 15, pos.y -11, 4293787648, 4278190080)
    elseif maxDamage >= target.health then
        Message.DrawTextWithBorder("Cooldown or need mana", 20, pos.x- 15, pos.y - 11, 4294967040, 4278190080)
    end
end

function OnTick()
    if not ball then 
        --print("z")
    end 


    if #Wpm:GetWayPoints(myHero) < 2 then lastmovement = nil end
    if not ball then OrbWalking.Enable(false) return end
    Sorting()

    if OriannaConfig.Basic.Combo then
        OrbWalking.Enable(true)
    else 
        OrbWalking.Enable(false)
    end 

    ts.range = qRange + 100
    ts:update()

    if not KillCombo() then 
        if not Farm() and (not OriannaConfig.Auto.AutoStop or OriannaConfig.Basic.Combo or OriannaConfig.Basic.Harass) then 
            if not Burst() then 
                Combo()
            end
        end 
    end 
    
    if ts.target  and OriannaConfig.Basic.Combo and OriannaConfig.Basic.SCombo and GetDistance(ts.target) < GetDistance(ts.target, ball) and GetDistance(ts.target, ball) > 1100 and GetDistance(ts.target) < 900 and myHero:CanUseSpell(_E) == READY then
        CastSpell(_E, myHero)
    end

    AutoShield()
end 

function KillCombo()
    if not OriannaConfig.Kill.killCombo or (not OriannaConfig.Basic.Combo and not OriannaConfig.Kill.autokillCombo) then return false end

    if not ball then return false end

    local range = qRange+ 50  < GetDistance(ball) + 50  and GetDistance(ball) + 50 or qRange+ 50

    for i, target in pairs(enemyHeroes) do
        if ValidTarget(target, range) then
            local extradmg = 0
            local qDmg = getDmg("Q", target, player)
            local wDmg = getDmg("W", target, player)
            local eDmg = getDmg("E", target, player)
            local rDmg = getDmg("R", target, player)
            local pDmg = getDmg("P", target, player)
            local aDmg = getDmg("AD",target, player)
            if ignite ~= nil and myHero:CanUseSpell(ignite) == READY  and GetDistance(target) < 600 then extradmg = extradmg + getDmg("IGNITE", target, player) end
            if GetInventorySlotItem(DFGSlot) and myHero:CanUseSpell(DFGSlot) == READY  and GetDistance(target) < 750 then extradmg = extradmg + getDmg("DFG", target, player) end
            if GetInventorySlotItem(HXGSlot) and myHero:CanUseSpell(DFGSlot) == READY  and GetDistance(target) < 700 then extradmg = extradmg + getDmg("HXG", target, player) end
            if GetInventorySlotItem(BWCSlot) and myHero:CanUseSpell(DFGSlot) == READY  and GetDistance(target) < 500 then extradmg = extradmg + getDmg("BWC", target, player) end

            local onspellDmg = (GetInventorySlotItem(liandry) and getDmg("LIANDRYS", target, player) or 0) + (GetInventorySlotItem(blackfire) and getDmg("BLACKFIRE", target, player) or 0)
            local onspellDmg = onspellDmg * 0.5
            local onhitDmg = pDmg + aDmg


            qDmg = qDmg + onspellDmg
            wDmg = wDmg + onspellDmg
            rDmg = rDmg + onspellDmg

            if (qDmg + eDmg + wDmg + rDmg + extradmg + onhitDmg) < target.health then break end

            if target.health < onhitDmg and GetDistance(target) < 500 then 
                OrbWalking.Attack(target)
            end

            if target.health < extradmg then 
                CastIgnite(target)
                CastItems(target)
                return true
            end

            if target.health < extradmg + onhitDmg and GetDistance(target) < 500 then 
                OrbWalking.Attack(target)
                CastIgnite(target)
                CastItems(target)
                return true
            end

             if target.health < (wDmg + extradmg) and CastW(target) then
                CastIgnite(target)
                CastItems(target)
                return true
            end

             if target.health < (wDmg + extradmg + onhitDmg) and GetDistance(target) < 500 and CastW(target) then
                OrbWalking.Attack(target)
                CastIgnite(target)
                CastItems(target)
                return true
            end 


            if target.health < (qDmg + extradmg) and CastQ(target) then
                CastIgnite(target)
                CastItems(target)
                return true
            end

            if target.health < (qDmg + extradmg + onhitDmg) and GetDistance(target) < 500 and CastQ(target) then
                OrbWalking.Attack(target)
                CastIgnite(target)
                CastItems(target)
                return true
            end

            if target.health < (qDmg + wDmg + extradmg) and ( myHero:GetSpellData(_Q).mana + myHero:GetSpellData(_W).mana)< myHero.mana and IsSpellReady(_W) and IsSpellReady(_Q) then 
                if CastW(target) then 
                    CastIgnite(target)
                    CastItems(target)
                    return true 
                elseif CastQ(target, tempballQ) then
                    return true
                end
            end

            if target.health < (qDmg + wDmg + extradmg + onhitDmg) and GetDistance(target) < 500 and ( myHero:GetSpellData(_Q).mana + myHero:GetSpellData(_W).mana)< myHero.mana and IsSpellReady(_W) and IsSpellReady(_Q) then 
                if CastW(target, tempballW) then 
                    OrbWalking.Attack(target)
                    CastIgnite(target)
                    CastItems(target)
                    return true 
                elseif CastQ(target, tempballQ) then
                    OrbWalking.Attack(target)
                    return true
                end
            end


            if OriannaConfig.Kill.RCombo and myHero:GetSpellData(_R).level > 0  and target.health < (qDmg + rDmg + extradmg) and ( myHero:GetSpellData(_Q).mana + myHero:GetSpellData(_R).mana)< myHero.mana and IsSpellReady(_Q) and myHero:CanUseSpell(_R) == READY then  
                if CastR(target, tempballR) then
                    CastIgnite(target)
                    CastItems(target)
                    return true
                elseif CastQ(target, tempballQ) then
                    return true
                end
            end

            if OriannaConfig.Kill.RCombo and myHero:GetSpellData(_R).level > 0  and target.health < (qDmg + rDmg + extradmg + onhitDmg) and  GetDistance(target) < 500 and ( myHero:GetSpellData(_Q).mana + myHero:GetSpellData(_R).mana)< myHero.mana and IsSpellReady(_Q) and myHero:CanUseSpell(_R) == READY then  
                if CastR(target, tempballR) then
                    OrbWalking.Attack(target)
                    CastIgnite(target)
                    CastItems(target)
                    return true
                elseif CastQ(target, tempballQ) then
                     OrbWalking.Attack(target)
                    return true
                end
            end

            if OriannaConfig.Kill.RCombo and myHero:GetSpellData(_R).level > 0  and target.health < (wDmg + rDmg + extradmg) and ( myHero:GetSpellData(_W).mana + myHero:GetSpellData(_R).mana)< myHero.mana and IsSpellReady(_W) and myHero:CanUseSpell(_R) == READY then  
                if CastR(target, tempballR) then
                    CastIgnite(target)
                    CastItems(target)
                    return true
                elseif CastW(target, tempballW) then 
                    CastIgnite(target)
                    CastItems(target)
                    return true
                end
            end

             if OriannaConfig.Kill.RCombo and myHero:GetSpellData(_R).level > 0  and target.health < (wDmg + rDmg + extradmg + onhitDmg) and GetDistance(target) < 500 and ( myHero:GetSpellData(_W).mana + myHero:GetSpellData(_R).mana)< myHero.mana and IsSpellReady(_W) and myHero:CanUseSpell(_R) == READY then  
                if CastR(target, tempballR) then
                    OrbWalking.Attack(target)
                    CastIgnite(target)
                    CastItems(target)
                    return true
                elseif CastW(target, tempballW) then 
                    OrbWalking.Attack(target)
                    CastIgnite(target)
                    CastItems(target)
                    return true
                end
            end

             if OriannaConfig.Kill.RCombo and myHero:GetSpellData(_R).level > 0 and target.health < (qDmg + wDmg + rDmg + extradmg) and ( myHero:GetSpellData(_Q).mana + myHero:GetSpellData(_W).mana + myHero:GetSpellData(_R).mana )< myHero.mana and IsSpellReady(_Q) and IsSpellReady(_W) and myHero:CanUseSpell(_R) == READY then 
                if CastR(target, tempballR) then
                    CastIgnite(target)
                    CastItems(target)
                    return true
                elseif CastW(target, tempballW) then
                    CastIgnite(target)
                    CastItems(target)
                    return true
                elseif CastQ(target, tempballQ) then 
                    return true
                end
            end

            if OriannaConfig.Kill.RCombo and myHero:GetSpellData(_R).level > 0  and target.health < (qDmg + wDmg + rDmg + extradmg + onhitDmg) and GetDistance(target) < 500 and ( myHero:GetSpellData(_Q).mana + myHero:GetSpellData(_W).mana + myHero:GetSpellData(_R).mana )< myHero.mana and IsSpellReady(_Q) and IsSpellReady(_W) and myHero:CanUseSpell(_R) == READY then 
                if CastR(target, tempballR) then
                    OrbWalking.Attack(target)
                    CastIgnite(target)
                    CastItems(target)
                    return true
                elseif CastW(target, tempballW) then
                    OrbWalking.Attack(target)
                    CastIgnite(target)
                    CastItems(target)
                    return true
                elseif CastQ(target, tempballQ) then 
                    OrbWalking.Attack(target)
                    return true
                end
            end



            local ally = CanCastE(target)        
            if ally ~= nil then    

                if target.health < (eDmg + extradmg) and ( myHero:GetSpellData(_E).mana)< myHero.mana and GetDistance(ally) < eRange  then 
                    if ally ~= nil then
                        CastSpell(_E, ally)
                        CastIgnite(target)
                        CastItems(target)
                        return true
                    end
                end

                if target.health < (eDmg + wDmg + extradmg) and ( myHero:GetSpellData(_E).mana + myHero:GetSpellData(_W).mana)< myHero.mana and IsSpellReady(_E) and myHero:CanUseSpell(_W) == READY then 
                   if ally ~= nil and CastW(target) then
                        CastIgnite(target)
                        CastItems(target)
                        return true
                   end 
                end
            end

            local E = InEComboRange(target)

            if E and OriannaConfig.Kill.RCombo and myHero:GetSpellData(_R).level > 0 then
                if target.health < (eDmg + rDmg + extradmg) and ( myHero:GetSpellData(_E).mana + myHero:GetSpellData(_R).mana)< myHero.mana and IsSpellReady(_E) and myHero:CanUseSpell(_R) == READY then 
                    if CastR(target) then
                        CastIgnite(target)
                        CastItems(target)
                        return true
                    end 
                end


                if target.health < (eDmg + wDmg + rDmg + extradmg) and ( myHero:GetSpellData(_E).mana + myHero:GetSpellData(_W).mana + myHero:GetSpellData(_R).mana )< myHero.mana and IsSpellReady(_E) and IsSpellReady(_W) and  myHero:CanUseSpell(_R) == READY then 
                    if CastR(target) then
                        CastIgnite(target)
                        CastItems(target)
                        return true
                    elseif CastW(target) then
                        CastIgnite(target)
                        CastItems(target)
                        return true
                    end 
                end
            end
        end
    end

    return false
end 

function TargetNear(object, distance, target)
    return object ~= nil and target ~= nil and object.valid and target.valid and object.networkID ~= target.networkID and not target.dead and not object.dead and target.bTargetable and object.bTargetable and GetDistance(target, object) <= distance
end

function IsSpellReady(Ispell)
    if myHero:CanUseSpell(_Q) == READY or (Cooldown[Ispell]-GetGameTimer() < 1.5 and Cooldown[Ispell]-GetGameTimer() > 0) then
        return true
    end 
    return false
end

function InEComboRange(target)
    for i, ally in pairs(allyHeroes) do
           if ally.valid and not ally.dead and ally.bTargetable and GetDistance(target,ally)<=rRange then
                
            local V = Vector(ally) - Vector(target)
            local k = V:normalized()
            local P = V:perpendicular2():normalized()

            local t,i,u = k:unpack()
            local x,y,z = P:unpack()


            local point1 = target.x + (x * (getHitBox(target)+35)) + (t * 825)
            local point2 = target.y + (y * (getHitBox(target)+35)) + (i * 825)
            local point3 = target.z + (z * (getHitBox(target)+35)) + (u * 825)

            local point4 = target.x + (x *(getHitBox(target)+35)) - (t * getHitBox(target))
            local point5 = target.y + (y *(getHitBox(target)+35)) - (i * getHitBox(target))
            local point6 = target.z + (z * (getHitBox(target)+35))  - (u * getHitBox(target))

            local lpoint1 = target.x - (x * (getHitBox(target)+35)) + (t * 825)
            local lpoint2 = target.y - (y * (getHitBox(target)+35))+ (i * 825)
            local lpoint3 = target.z - (z * (getHitBox(target)+35))+ (u * 825)

            local lpoint4 = target.x - (x * (getHitBox(target)+35))- (t * getHitBox(target))
            local lpoint5 = target.y - (y * (getHitBox(target)+35))- (i * getHitBox(target))
            local lpoint6 = target.z - (z * (getHitBox(target)+35)) - (u * getHitBox(target))

            local c = WorldToScreen(D3DXVECTOR3(point1, point2, point3))
            local l = WorldToScreen(D3DXVECTOR3(point4, point5, point6))
            local cl = WorldToScreen(D3DXVECTOR3(lpoint1, lpoint2, lpoint3))
            local ll = WorldToScreen(D3DXVECTOR3(lpoint4, lpoint5, lpoint6))

            local poly = Polygon(Point(c.x, c.y),  Point(l.x, l.y), Point(cl.x, cl.y),   Point(ll.x, ll.y))


            local b = WorldToScreen(D3DXVECTOR3(ball.x, ball.y, ball.z))
            local po = Point(b.x, b.y)
            if poly:contains(po) then
                return true
            end
        end
    end
    return false
end

function OnRecall(unit, channelTimeInMs)    -- gets triggered when somebody starts to recall
    if unit and unit.isMe then
        IsRecaling = true
    end
end
function OnAbortRecall(unit)                -- gets triggered when somebody aborts a recall
    if unit and unit.isMe then
        IsRecaling = false
    end 
end
function OnFinishRecall(unit)               -- gets triggered when somebody finishes a recall
    if unit and unit.isMe then
        IsRecaling = false
    end
end


function CanCastE(target)

     for i, ally in pairs(allyHeroes) do
        if ally.valid and not ally.dead and ally.bTargetable and GetDistance(target,ally)<=eRange then
                
            local V = Vector(target) - Vector(ally)
            local k = V:normalized()
            local P = V:perpendicular2():normalized()

            local t,i,u = k:unpack()
            local x,y,z = P:unpack()


            local point1 = target.x + (x * (getHitBox(target)+35)) + (t * 825)
            local point2 = target.y + (y * (getHitBox(target)+35)) + (i * 825)
            local point3 = target.z + (z * (getHitBox(target)+35)) + (u * 825)

            local point4 = target.x + (x *(getHitBox(target)+35)) - (t * getHitBox(target))
            local point5 = target.y + (y *(getHitBox(target)+35))- (i * getHitBox(target))
            local point6 = target.z + (z * (getHitBox(target)+35)) - (u * getHitBox(target))

            local lpoint1 = target.x - (x * (getHitBox(target)+35)) + (t * 825)
            local lpoint2 = target.y - (y * (getHitBox(target)+35))+ (i * 825)
            local lpoint3 = target.z - (z * (getHitBox(target)+35))+ (u * 825)

            local lpoint4 = target.x - (x * (getHitBox(target)+35))- (t * getHitBox(target))
            local lpoint5 = target.y - (y * (getHitBox(target)+35))- (i * getHitBox(target))
            local lpoint6 = target.z - (z * (getHitBox(target)+35)) - (u * getHitBox(target))

            local c = WorldToScreen(D3DXVECTOR3(point1, point2, point3))
            local l = WorldToScreen(D3DXVECTOR3(point4, point5, point6))
            local cl = WorldToScreen(D3DXVECTOR3(lpoint1, lpoint2, lpoint3))
            local ll = WorldToScreen(D3DXVECTOR3(lpoint4, lpoint5, lpoint6))

            local poly = Polygon(Point(c.x, c.y),  Point(l.x, l.y), Point(cl.x, cl.y),   Point(ll.x, ll.y))


            local b = WorldToScreen(D3DXVECTOR3(ball.x, ball.y, ball.z))
            local po = Point(b.x, b.y)
            if poly:contains(po) then
                return ally
            end
        end
    end
    return nil
end

function CastQCallback(target)
    if not bMoving and myHero:GetSpellData(_Q).mana < myHero.mana and myHero:CanUseSpell(_Q) == READY and ball and GetDistance(ball, target) < qRange+100 then
        prodictionQ:EnableTarget(target, true, ball)
        return true
    end 
    return false
end 

function CastQ(enemy)
    if not bMoving and myHero:CanUseSpell(_Q) == READY and myHero:GetSpellData(_Q).mana < myHero.mana and ball and GetDistance(enemy, ball) < qRange+100 then
        local pos, t, hitchance =  prodictionQ:GetPrediction(enemy)
        if pos ~= nil then 
            if GetDistance(pos, ball) < qRange + 50 then
                CastSpell(_Q, pos.x, pos.z)
                return true
            end
        end
    end 
    return false
end 

function CastW(enemy)
    if not bMoving and ball and myHero:CanUseSpell(_W) == READY and myHero:GetSpellData(_W).mana < myHero.mana then
        if GetDistance(enemy, ball) < 250 then
            CastSpell(_W)
            return true
        end
    end 
    return false
end 

function CastR(enemy)
    if not bMoving and myHero:CanUseSpell(_R) == READY and myHero:GetSpellData(_R).mana < myHero.mana and GetDistance(enemy, ball) < rRange+200 then
        local pos, t, hitchance = prodictionR:GetPrediction(enemy)
        if pos ~= nil then
            if GetDistance(pos, ball) < rRange then 
                CastSpell(_R)
                return true
            end
        end
            
    end
    return false
end 

function CastIgnite(target)
    if ignite ~= nil and myHero:CanUseSpell(ignite) == READY and GetDistance(target) < 600 then
        CastSpell(ignite, target)
        return true
    end
    return false
end

function CastItems(target)
    if GetInventorySlotItem(DFGSlot) and myHero:CanUseSpell(DFGSlot) == READY  and GetDistance(target) < 750 then
        CastSpell(DFGSlot, target)
    end 
    if GetInventorySlotItem(HXGSlot) and myHero:CanUseSpell(HXGSlot) == READY  and GetDistance(target) < 700 then
        CastSpell(HXGSlot, target)
    end 
    if GetInventorySlotItem(BWCSlot) and myHero:CanUseSpell(BWCSlot) == READY  and GetDistance(target) < 500 then
        CastSpell(BWCSlot, target)
    end  


    if ignite ~= nil and myHero:CanUseSpell(ignite) == READY and GetDistance(target) < 600 then
        CastSpell(ignite, target)
        return true
    end
end

function Burst()
    if not OriannaConfig.Basic.Burst or not ts.target or bMoving then return false end 

    if  GetDistance(ts.target) <= myHero.range + getHitBox(ts.target) + 100 and OrbWalking.CanAttack() and GetDistance(ts.target) > 300 then 
        OrbWalking.Attack(ts.target)
        return true 
    end   

    if GetDistance(ts.target, ball) < qRange then 
        CastQCallback(ts.target)   
        return true 
    end

    if GetDistance(ts.target, ball) < wRange then 
        CastW(ts.target)
        return true 
    end 

    if myHero:CanUseSpell(_E) == READY and myHero:GetSpellData(_E).mana < myHero.mana then
        local ally = CanCastE(ts.target)
        if objectValid(ally) and GetDistance(ally) < eRange then
            CastSpell(_E, ally)
            return true
        end
    end

    if myHero:CanUseSpell(_R) == READY and myHero:GetSpellData(_R).level > 0 then 
        local V = Vector(ball) - Vector(myHero)
        local k = V:normalized()
        local P = V:perpendicular2():normalized()

        local t,i,u = k:unpack()
        local x,y,z = P:unpack()

        local point1 = ball.x + (x * 120) + (t * rRange)
        local point2 = ball.y + (y * 120) + (i * rRange)
        local point3 = ball.z + (z * 120) + (u * rRange)

        local point4 = ball.x + (x * 120) + (t * 120)
        local point5 = ball.y + (y *120) + (i * 120)
        local point6 = ball.z + (z * 120)  + (u * 120)

        local lpoint1 = ball.x - (x * 120) + (t * rRange)
        local lpoint2 = ball.y - (y * 120)+ (i * rRange)
        local lpoint3 = ball.z - (z * 120)+ (u * rRange)

        local lpoint4 = ball.x - (x * 120)+ (t * 120)
        local lpoint5 = ball.y - (y * 120)+ (i * 120)
        local lpoint6 = ball.z - (z * 120) +(u * 120)

        local c = WorldToScreen(D3DXVECTOR3(point1, point2, point3))
        local l = WorldToScreen(D3DXVECTOR3(point4, point5, point6))
        local cl = WorldToScreen(D3DXVECTOR3(lpoint1, lpoint2, lpoint3))
        local ll = WorldToScreen(D3DXVECTOR3(lpoint4, lpoint5, lpoint6))

        local poly = Polygon(Point(c.x, c.y),  Point(l.x, l.y), Point(cl.x, cl.y),   Point(ll.x, ll.y))


        local b = WorldToScreen(D3DXVECTOR3(ball.x, ball.y, ball.z))
        local po = Point(b.x, b.y)
        if poly:contains(po) then
            CastSpell(_R)
            return true
        end
    end 


    return false 
end 

function Combo()

    if ts.target ~= nil then
        if (OriannaConfig.Basic.Combo or OriannaConfig.Basic.Harass or (not OriannaConfig.Auto.AutoStop and OriannaConfig.Auto.AutoQ and myHero.maxMana*(OriannaConfig.Auto.AutoQpMana/100) < myHero.mana and myHero.maxHealth*(OriannaConfig.Auto.Autohealt/100) < myHero.health)) and myHero:CanUseSpell(_Q) == READY then
            CastQCallback(ts.target)  
            return true      
        end        

        if (OriannaConfig.Basic.Combo or OriannaConfig.Basic.Harass) and GetDistance(ts.target, ball) < wRange and myHero:CanUseSpell(_W) == READY then
            CastW(ts.target)
            return true
        end 
    end 

    local wCount = 0
    local rCount = 0
    local eCast = nil
    local enemyaround = 0
    for i, target in pairs(enemyHeroes) do
        local ran = ts.range > eRange and ts.range or eRange
        if ValidTarget(target, ran) then
            if (OriannaConfig.Basic.Combo or OriannaConfig.Basic.Harass or (not OriannaConfig.Auto.AutoStop and OriannaConfig.Auto.AutoQ and myHero.maxMana*(OriannaConfig.Auto.AutoQpMana/100) < myHero.mana and myHero.maxHealth*(OriannaConfig.Auto.Autohealt/100) < myHero.health)) and myHero:CanUseSpell(_Q) == READY then
            CastQCallback(target)  
            return true      
        end        

        if (OriannaConfig.Basic.Combo or OriannaConfig.Basic.Harass) and GetDistance(target, ball) < wRange and myHero:CanUseSpell(_W) == READY then
            CastW(target)
            return true
        end 


            if myHero:CanUseSpell(_W) == READY and (not OriannaConfig.Auto.AutoStop and OriannaConfig.Auto.AutoW and myHero.maxMana*(OriannaConfig.Auto.AutoWpMana/100) < myHero.mana and myHero.maxHealth*(OriannaConfig.Auto.Autohealt/100) < myHero.health) and GetDistance(target, ball) < wRange then
                wCount = wCount + 1
            end 

             if myHero:CanUseSpell(_R) == READY and myHero:GetSpellData(_R).mana < myHero.mana and (OriannaConfig.Basic.Combo or (not OriannaConfig.Auto.AutoStop and OriannaConfig.Auto.AutoR)) and GetDistance(target, ball) < rRange+50 then
                rCount = rCount + 1
             end

            if myHero:CanUseSpell(_E) == READY and not eCast and myHero:GetSpellData(_E).mana < myHero.mana and OriannaConfig.Basic.Combo and GetDistance(target, ball) > wRange and GetDistance(target) > qRange then
                for j, ally in pairs(allyHeroes) do
                    if GetDistance(ally, target) <= wRange and GetDistance(ally) < eRange then 
                        eCast = ally
                    end
                end
            end 

            if myHero:CanUseSpell(_E) == READY and myHero:GetSpellData(_E).mana < myHero.mana and (OriannaConfig.Basic.Combo or OriannaConfig.Shield.shielddmg) then
                local ally = CanCastE(target)
                if objectValid(ally) and GetDistance(ally) < eRange then
                    CastSpell(_E, ally)
                    return true
                end
            end
        end 
    end 
    if not OriannaConfig.Auto.AutoStop and OriannaConfig.Auto.AutoWenemy <= wCount and 1 <= wCount then
        CastSpell(_W)
        return true
    end

    if (OriannaConfig.Basic.Combo or OriannaConfig.Basic.Harass or not OriannaConfig.Auto.AutoStop) and OriannaConfig.Auto.AutoRenemy <= rCount and 1 <= rCount then
        CastSpell(_R)
        return true
    end

    if eCast then 
        CastSpell(_E, eCast)
        return true
    end 
end 

function Farm()
    if OriannaConfig.Farm.Farm and not OriannaConfig.Basic.Combo and not OriannaConfig.Basic.Harass and myHero.maxMana*(OriannaConfig.Farm.FarmpMana/100) < myHero.mana and (OriannaConfig.Farm.UseW or OriannaConfig.Farm.UseQ) then
        
        if not ball then return end 
        minions = Minions({team = {TEAM_ENEMY}, range = qRange, bTargetable = true, minms = 50})
            table.sort(minions, function(x, y) 
                local dmgx = myHero:CalcDamage(x, myHero.totalDamage)
                local dmgy = myHero:CalcDamage(y, myHero.totalDamage)
                local valuex = x.health/dmgx
                local valuey = y.health/dmgy

                return valuex < valuey
            end)

        local QPrediction = TargetPredictionVIP(900, 1200, 0.250, 80, ball)
        local WPrediction = TargetPredictionVIP(wRange, 20000, 0.3, nil, ball)
        
        for index, minion in ipairs(minions) do
            local wDmg = getDmg("W", minion, player)
            local qDmg = getDmg("Q", minion, player) 

            if OriannaConfig.Farm.UseW and minion.health < wDmg and GetDistance(ball, minion) < wRange and myHero:CanUseSpell(_W) == READY then
                local pos1, t1, vec1 = WPrediction:GetPrediction(minion)
                if pos1 then 
                    CastSpell(_W)
                end 
            elseif OriannaConfig.Farm.UseQ and minion.health < qDmg and GetDistance(minion) < qRange and myHero:CanUseSpell(_Q) == READY then
                local pos1, t1, vec1 = QPrediction:GetPrediction(minion)
                if pos1 then 
                    CastSpell(_Q, pos1.x, pos1.z)
                end 
            end
            return false
        end
    end
    return false

end